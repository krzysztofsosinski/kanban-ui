import * as React from "react";
import {splitFormProps, useField} from "react-form";
import {CircularProgress, InputAdornment, TextField} from "@material-ui/core";

export function InputField(props) {
    const [field, fieldOptions, rest] = splitFormProps(props);

    const {
        value = '',
        meta: {error, isTouched, isValidating},
        getInputProps
    } = useField(field, fieldOptions);

    const isError = isTouched && Boolean(error);

    let validationProps = {
        error: isError,
        helperText: isError ? error : ""
    };

    return (
        <TextField {...getInputProps({...rest, ...validationProps})}
                   InputProps={{
                       endAdornment: isValidating &&
                           <InputAdornment position={"end"}><CircularProgress size={'1em'}/></InputAdornment>
                   }}>
        </TextField>
    )
}

export default InputField;