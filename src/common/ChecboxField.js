import * as React from "react";
import {splitFormProps, useField} from "react-form";
import {Checkbox, FormControlLabel} from "@material-ui/core";

const CheckboxField = React.forwardRef((props, ref) => {
    const [field, fieldOptions, rest] = splitFormProps(props);

    const {
        value = false,
        setValue,
    } = useField(field, fieldOptions);

    const handleSelectionChange = (e) => {
        setValue(e.target.checked);
    };

    const {label, ...restInputProps} = rest;

    return (
        <FormControlLabel
            control={<Checkbox {...restInputProps} onChange={handleSelectionChange} checked={value}/>}
            label={label}
        />
    )
});

export default CheckboxField;