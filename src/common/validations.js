export function isNotEmpty(msg) {
    return (value) => !value ? msg : false;
}