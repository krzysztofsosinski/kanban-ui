import * as React from "react";
import {splitFormProps, useField} from "react-form";
import {Chip, CircularProgress, InputAdornment, makeStyles, TextField, useTheme} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";

const useStyle = makeStyles({
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
}, {name: 'SelectField'});

function getStyles(value, values, theme) {
    return {
        fontWeight:
            values.indexOf(value) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

export default function SelectField(props) {
    const classes = useStyle();
    const theme = useTheme();
    const [field, fieldOptions, rest] = splitFormProps(props);
    const {options, multiple = false} = rest;

    console.log('multiple', multiple);

    const {
        value = multiple ? [] : '',
        meta: {error, isTouched, isValidating},
        setValue,
        getInputProps
    } = useField(field, fieldOptions);

    const isError = isTouched && Boolean(error);

    const handleSelectChange = e => {
        setValue(e.target.value);
    };

    let validationProps = {
        error: isError,
        helperText: isError ? error : ""
    };

    const selectPops = {
        multiple,
        renderValue: multiple ? (selected) => (
            <div className={classes.chips}>
                {selected.map(value => (
                    <Chip key={value} label={value} className={classes.chip}/>
                ))}
            </div>
        ) : null
    };

    return (
        <TextField select SelectProps={{...selectPops}} {...getInputProps({...{...rest, ...validationProps}})}
                   InputProps={{
                       endAdornment: isValidating &&
                           <InputAdornment position={"end"}><CircularProgress size={'1em'}/></InputAdornment>
                   }} onChange={handleSelectChange}>
            {
                options.map(opt => (
                    <MenuItem key={opt.value} value={opt.value} style={multiple ? getStyles(opt, value, theme) : {}}>
                        {opt.label}
                    </MenuItem>
                ))
            }
        </TextField>
    )
};
