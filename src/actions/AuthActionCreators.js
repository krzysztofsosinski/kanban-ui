import KanbanAPI from "../api/KanbanApi";

export const AUTH_USER = "AUTH_USER";
export const AUTH_ERROR = "AUTH_ERROR";
export const UNAUTH_USER = "UNAUTH_USER";

const AuthActionsCreator = {

    singInUser: function ({email, password}) {
        return function (dispatch) {

            const request = KanbanAPI.singIn(email, password);
            request
                .then(response => {
                    localStorage.setItem('token', response.data.accessToken);
                    localStorage.setItem('refreshToken', response.data.refreshToken);
                    dispatch({type: AUTH_USER})
                })
                .catch(() => {
                    dispatch(AuthActionsCreator.authError('bad login info'))
                })

        }
    },
    singUpUser: function ({firstName, lastName, email, password, passwordConfirmation}) {
        return function (dispatch) {
            KanbanAPI.singUp({firstName, lastName, email, password, passwordConfirmation})
                .then(response => {
                    localStorage.setItem('token', response.data.accessToken);
                    localStorage.setItem('refreshToken', response.data.refreshToken);
                    dispatch({type: AUTH_USER});
                })
                .catch(({response}) => {
                    dispatch(AuthActionsCreator.authError(response.data.message))
                })
        }
    },
    signOutUser: function () {
        localStorage.removeItem('token');
        return {
            type: UNAUTH_USER
        }
    },
    authError: function (error) {
        return {
            type: AUTH_ERROR,
            payload: error
        }
    }
};

export default AuthActionsCreator;