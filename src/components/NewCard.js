import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CardForm from './CardForm';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import CardActionCreators from '../actions/CardActionCreators';
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {DialogActions} from "@material-ui/core";
import Button from "@material-ui/core/Button";

class NewCard extends Component {

    formRef = React.createRef();

    handleChange = (field, value) => {
        this.props.updateDraft(field, value);
    };

    handleSubmit = (e) => {
        const {addCard, history, draft} = this.props;
        e.preventDefault();
        addCard(draft);
        history.push('/board');
    };

    handleClose = () => {
        this.props.history.push('/board');
    };

    componentDidMount() {
        const self = this;
        setTimeout(() => self.props.createDraft(), 0);
    }

    render() {
        console.log('formR', this.formRef);
        return (
            <Dialog open={true} onClose={this.handleClose}>
                <DialogTitle>Create new card</DialogTitle>
                <DialogContent>
                    <CardForm ref={this.formRef} draftCard={this.props.draft} buttonLabel="Create card"
                              handleChange={this.handleChange} handleSubmit={this.handleSubmit}
                              handleClose={this.handleClose}/>
                </DialogContent>
                <DialogActions>
                    <Button>Submit</Button>
                </DialogActions>
            </Dialog>
        );
    }
}

NewCard.propTypes = {
    draft: PropTypes.object,
    createDraft: PropTypes.func.isRequired,
    updateDraft: PropTypes.func.isRequired,
    addCard: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
        draft: state.cardDraft
    }
);

const mapDispatchToProps = (dispatch) => (
    {
        createDraft: () => dispatch(CardActionCreators.createDraft()),
        updateDraft: (field, value) => dispatch(CardActionCreators.updateDraft(field, value)),
        addCard: (draft) => dispatch(CardActionCreators.addCard(draft))
    }
);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewCard));