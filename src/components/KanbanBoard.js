import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {DragDropContext} from 'react-dnd';
import {connect} from 'react-redux';
import HTML5Backend from 'react-dnd-html5-backend';
import List from './List';
import {Link, Route, withRouter} from 'react-router-dom';
import CardActionCreators from '../actions/CardActionCreators';
import NewCard from "./NewCard";

class KanbanBoard extends Component {

    componentDidMount() {
        this.props.fetchCards();
    }

    render() {
        console.log(this.props);
        const url = this.props.match.url;
        return (
            <div className="app">
                <Link to={`${url}/new`} className="float-button">+</Link>
                <List id='todo' title="To Do" cards={this.props.cards.filter((card) => card.status === "todo")}/>
                <List id='in-progress' title="In progress"
                      cards={this.props.cards.filter((card) => card.status === "in-progress")}/>
                <List id='done' title='Done' cards={this.props.cards.filter((card) => card.status === "done")}/>
                {this.props.children}
                <Route path={`${url}/new`} component={NewCard}/>
            </div>
        );
    }
}

KanbanBoard.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.object),
    fetchCards: PropTypes.func
};

const KanbanWithDragDrop = DragDropContext(HTML5Backend)(KanbanBoard);

const mapStateToProps = (state) => (
    {
        cards: state.cards
    }
);

const mapDispatchToProps = (dispatch) => (
    {
        fetchCards: () => dispatch(CardActionCreators.fetchCards())
    }
);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(KanbanWithDragDrop));
