import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

const styles = {
    root: {
        flexGrow: 1,
    },
};

const HomeLink = props => <Link to="/board" {...props} />;
const SignOutLink = props => <Link to="/signout" {...props}/>;
const SignInLink = props => <Link to="/signin" {...props}/>;
const SignUpLink = props => <Link to="/singup" {...props}/>;
const KanbanBoardLink = props => <Link to="/board" {...props}/>;

const Header = ({authenticated, classes}) => (
    <div className={classes.root}>
        <AppBar position={"static"}>
            {authenticated ?
                (
                    <Toolbar>
                        <Button variant={"outlined"} size={"small"} component={HomeLink}>Home</Button>
                        <Button variant={"outlined"} size={"small"} component={KanbanBoardLink}>Kanban board</Button>
                        <Button variant={"outlined"} size={"small"} component={SignOutLink}>Sign Out</Button>
                    </Toolbar>
                )
                :
                (<Toolbar>
                    <Button variant={"outlined"} size={"small"} component={HomeLink}>Home</Button>
                    <Button variant={"outlined"} size={"small"} component={SignInLink}>Sign In</Button>
                    <Button variant={"outlined"} size={"small"} component={SignUpLink}>Sign Up</Button>
                </Toolbar>)
            }
        </AppBar>
    </div>
);

Header.propTypes = {
    authenticated: PropTypes.bool,
    classes: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
    return {
        authenticated: state.auth.authenticated
    }
};

export default connect(mapStateToProps, null)(withStyles(styles)(Header));