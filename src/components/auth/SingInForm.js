import React from 'react';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import {Link as RouterLink} from "react-router-dom";
import Link from "@material-ui/core/Link";
import {makeStyles} from "@material-ui/styles";
import {useForm} from "react-form";
import InputField from "../../common/InputField";
import CheckboxField from "../../common/ChecboxField";

const ForgetPasswordLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref}
                                                                        to="/forgetpassword" {...props}/>);
const SignUpLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} to="/singup" {...props}/>);

const formStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}), {name: 'SignInForm'});

export default function SignInForm({errorMessage, onSubmit}) {
    const classes = formStyles();

    const {
        Form, meta: {isSubmitting, canSubmit}
    } = useForm({
        onSubmit: async (values) => onSubmit(values)
    });

    const defaultValueForTextField = React.useMemo(() => "", []);

    return <>
        {errorMessage && <div className="alert alert-danger"><strong>Oops: </strong>{errorMessage} </div>}
        <Form className={classes.root}>
            <InputField defaultValue={defaultValueForTextField} variant="outlined" margin="normal" label="Email Address"
                        fullWidth
                        required field="email" autoFocus autoComplete="email"/>

            <InputField defaultValue={defaultValueForTextField} variant="outlined" margin="normal" label="Password"
                        fullWidth
                        required field="password" type="password" autoFocus autoComplete="email"/>

            <CheckboxField field="rememberme" label="Remember me"/>

            <Button type="submit"
                    fullWidth
                    disabled={!canSubmit}
                    variant="contained"
                    color="primary"
                    className={classes.submit}>
                Sign in
            </Button>

            <Grid container>
                <Grid item xs>
                    <Link component={ForgetPasswordLink} variant="body2">
                        Forgot password?
                    </Link>
                </Grid>
                <Grid item>
                    <Link component={SignUpLink} variant="body2">
                        Don't have an account? Sign Up
                    </Link>
                </Grid>
            </Grid>
        </Form>
    </>
};