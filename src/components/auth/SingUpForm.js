import React from 'react';
import Button from '@material-ui/core/Button';
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import {useForm} from "react-form";
import InputField from "../../common/InputField";
import {isNotEmpty} from "../../common/validations";

const formStyles = makeStyles(theme => ({
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}), {name: 'SignUpForm'});

export default function SignUpForm({errorMessage, onSubmit}) {
    const classes = formStyles();

    const {
        Form, values, meta: {canSubmit}
    } = useForm({
        onSubmit: async (values) => onSubmit(values),
    });

    const defaultValueForTextField = React.useMemo(() => "", []);

    return (
        <>
            {errorMessage && <div className="alert alert-danger">
                <strong>Oops: </strong>{this.props.errorMessage}
            </div>}
            <Form className={classes.form}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <InputField defaultValue={defaultValueForTextField}
                                    autoComplete="fname" label="First Name"
                                    validate={isNotEmpty('Please enter first name')}
                                    field="firstName" fullWidth variant="outlined" required
                                    autoFocus/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <InputField
                            defaultValue={defaultValueForTextField} autoComplete="lname"
                            validate={isNotEmpty('Please enter last name')}
                            label="Last Name" field="lastName" fullWidth variant="outlined" required/>
                    </Grid>

                    <Grid item xs={12}>
                        <InputField defaultValue={defaultValueForTextField}
                                    label="Email Address" field="email" fullWidth
                                    validate={isNotEmpty('Please enter email')}
                                    variant="outlined" required autoComplete="email"/>
                    </Grid>

                    <Grid item xs={12}>
                        <InputField defaultValue={defaultValueForTextField}
                                    variant="outlined" label="Password" field="password"
                                    validate={isNotEmpty('Please enter password')}
                                    fullWidth required autoComplete="current-password"/>
                    </Grid>
                    <Grid item xs={12}>
                        <InputField defaultValue={defaultValueForTextField} placeholder={'Password confirmation'}
                                    variant="outlined" label="Password Confirmation" field="passwordConfirmation"
                                    validate={(value) => {
                                        return !value || value !== values.password ? 'Please confirm your password' : false;
                                    }}
                                    fullWidth required type="password"/>
                    </Grid>
                </Grid>
                <Button fullWidth disabled={!canSubmit} variant="contained"
                        color="primary" type="submit" className={classes.submit}>
                    Sign up
                </Button>
            </Form>
        </>
    )
}