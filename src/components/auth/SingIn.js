import React, {Component} from 'react';
import SignInForm from './SingInForm';
import AuthActionCreators from '../../actions/AuthActionCreators';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {Container} from "@material-ui/core";
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {withStyles} from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
});

class SingIn extends Component {

    componentWillUnmount() {
        if (this.props.errorMessage) {
            this.props.authError(null);
        }
    }

    displayRedirectMessages() {
        const location = this.props.location;
        return location.state && <div className="alert alert-danger">{location.state.message}</div>
    }

    handleSubmit = ({email, password}) => {
        this.props.singInUser({email, password})
    };

    getRedirectPath() {
        const locationState = this.props.location.state;
        return locationState && locationState.from.pathname ? locationState.from.pathname : '/';
    }

    render() {
        const {classes} = this.props;

        return (this.props.authenticated) ?
            <Redirect to={{
                pathname: this.getRedirectPath(), state: {
                    from: this.props.location
                }
            }}/>
            :
            <Container component='main' maxWidth='xs'>
                <div className={classes.paper}>
                    {this.displayRedirectMessages()}
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component='h1' variant='h5'>
                        Sign in
                    </Typography>
                    <SignInForm onSubmit={this.handleSubmit} errorMessage={this.props.errorMessage}/>
                </div>
            </Container>
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated,
        errorMessage: state.auth.error
    }
}

const mapDisptachToProps = (dispatch) => {
    return bindActionCreators({
        singInUser: AuthActionCreators.singInUser,
        authError: AuthActionCreators.authError
    }, dispatch)
};

export default connect(mapStateToProps, mapDisptachToProps)(withStyles(styles)(SingIn));