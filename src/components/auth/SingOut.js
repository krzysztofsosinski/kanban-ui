import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AuthActionsCreator from '../../actions/AuthActionCreators';

class SignOut extends Component {

    componentWillMount() {
        this.props.signOutUser()
    }

    render() {
        return <div>Bye Bye</div>
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        signOutUser: AuthActionsCreator.signOutUser
    }, dispatch)
};

export default connect(null, mapDispatchToProps)(SignOut);