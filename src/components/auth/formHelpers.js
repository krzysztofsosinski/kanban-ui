import React from 'react'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {Checkbox} from "@material-ui/core";

export const renderTextField = ({input, type, label, meta: {touched, error}, ...custom}) => (
    <div>
        <TextField
            type={type}
            label={label}
            {...input}
            {...custom}
        />
        {touched && error && <span className="error">{error}</span>}
    </div>
);

export const renderCheckbox = ({input, type, label, meta: {touched, error}, ...custom}) => (
    <div>
        <FormControlLabel
            control={<Checkbox value={"remember"} color={"primary"} {...input}
                               {...custom} />}
            label={label}
        />
        {touched && error && <span className="error">{error}</span>}
    </div>
);
