import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import SingUpForm from './SingUpForm';
import AuthActionCreators from '../../actions/AuthActionCreators';
import {Redirect} from 'react-router-dom';
import {withStyles} from "@material-ui/styles";
import {Container} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Avatar from "@material-ui/core/Avatar";

const styles = theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
});

class SignUp extends Component {

    componentWillUnmount() {
        if (this.props.errorMessage) {
            this.props.authError(null)
        }
    }

    handleSubmit = ({firstName, lastName, email, password, passwordConfirmation}) => {
        this.props.singUpUser({firstName, lastName, email, password, passwordConfirmation})
    };

    getRedirectPath() {
        const locationState = this.props.location.state;
        return locationState && locationState.from.pathname ?
            locationState.from.pathname : '/';
    }

    render() {
        const {classes} = this.props;

        return (this.props.authenticated) ?
            <Redirect to={{
                pathname: this.getRedirectPath(), state: {
                    from: this.props.location
                }
            }}/>
            :
            <Container component="main" maxWidth="xs">
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component='h1' variant='h5'>
                        Sign up
                    </Typography>
                    <SingUpForm onSubmit={this.handleSubmit} errorMessage={this.props.errorMessage}/>
                </div>
            </Container>
    }
}

const mapStateToProps = (state) => {
    return {
        authenticated: state.auth.authenticated,
        errorMessage: state.auth.error
    }
};

const mapDisptachToProps = (dispatch) => {
    return bindActionCreators({
        authError: AuthActionCreators.authError,
        singUpUser: AuthActionCreators.singUpUser
    }, dispatch)
};

export default connect(mapStateToProps, mapDisptachToProps)(withStyles(styles)(SignUp));