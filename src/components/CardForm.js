import React, {forwardRef, useImperativeHandle, useMemo} from 'react';
import PropTypes from 'prop-types';
import {useForm} from "react-form";
import Grid from "@material-ui/core/Grid";
import InputField from "../common/InputField";
import SelectField from "../common/SelectField";

const CardForm = forwardRef(function CardForm({draftCard}, ref) {

    const {
        Form, handleSubmit,
    } = useForm({
        onSubmit: (values) => {
            console.log('submit')
        }
    });

    const options = useMemo(() => [
        {
            value: "todo",
            label: "To do"
        },
        {
            value: "in-progress",
            label: "In progress"
        },
        {
            value: "done",
            label: "Done"
        }
    ], []);

    useImperativeHandle(ref, () => ({
        handleSubmit
    }));

    return (
        <Form>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <InputField value={draftCard.title} autoFocus
                                field="title" fullWidth label="Title" required/>
                </Grid>
                <Grid item xs={12}>
                    <InputField value={draftCard.description} multiline rows={4}
                                field="description" fullWidth label="Description" required/>
                </Grid>
                <Grid item xs={12}>
                    <SelectField value={draftCard.status}
                                 field="status" fullWidth label="Status" options={options} required/>
                </Grid>
                <Grid item xs={12}>
                    <InputField value={draftCard.color} type="color" field="color" fullWidth label="Color" required/>
                </Grid>
            </Grid>
        </Form>
    )
});

CardForm.propTypes = {
    buttonLabel: PropTypes.string.isRequired,
    draftCard: PropTypes.shape({
        title: PropTypes.string,
        description: PropTypes.string,
        status: PropTypes.string,
        color: PropTypes.string
    }).isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
};

export default CardForm;