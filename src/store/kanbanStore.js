import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import logger from 'redux-logger'

const kanbanStore = createStore(
    reducers,
    applyMiddleware(thunk, logger)
);

export default kanbanStore;