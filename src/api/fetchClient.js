import axios from 'axios';
import KanbanAPI from "./KanbanApi";

const API_URL = 'http://localhost:8090';

function isTokenExpiredError(responseError) {
    return responseError.status === 401;
}

let isAlreadyFetchingAccessToken = false;
let subscribers = [];

async function resetTokenAndReattemptRequest(error) {
    try {
        const {response: errorResponse} = error;
        const refreshToken = localStorage.getItem("refreshToken");

        if (!refreshToken) {
            return Promise.reject(error);
        }

        const retryOriginalRequest = new Promise(resolve => {
            addSubscriber(accessToken => {
                errorResponse.config.headers.Authorization = `Bearer ${accessToken}`;
                resolve(axios(errorResponse.config));
            });
        });

        if (!isAlreadyFetchingAccessToken) {
            isAlreadyFetchingAccessToken = true;
            const response = await KanbanAPI.refresh(refreshToken);

            if (!response.data) {
                return Promise.reject(error);
            }

            const newToken = response.data.accessToken;
            localStorage.setItem('accessToken', response.data.accessToken);
            isAlreadyFetchingAccessToken = false;
            onAccessTokenFetched(newToken);
        }
        return retryOriginalRequest;
    } catch (err) {
        return Promise.reject(err);
    }
}

function onAccessTokenFetched(accessToken) {
    // When the refresh is successful, we start retrying the requests one by one and empty the queue
    while (subscribers.length) {
        subscribers.shift()(accessToken);
    }
}

function addSubscriber(callback) {
    subscribers.push(callback);
}

const fetchClient = () => {
    const defaultOptions = {
        baseURL: API_URL,
        timeout: 1000,
        headers: {
            'Content-Type': 'application/json',
        },
        maxContentLength: 200000,
    };

    let instance = axios.create(defaultOptions);

    instance.interceptors.request.use((config) => {
        const token = localStorage.getItem('accessToken');
        config.headers.Authorization = token ? `Bearer ${token}` : '';
        return config;
    });

    instance.interceptors.response.use((response) => response, (error) => {
        const errorResponse = error.response;
        if (isTokenExpiredError(errorResponse)) {
            return resetTokenAndReattemptRequest(error);
        }
        return Promise.reject(error);
    });

    return instance;
};

export default fetchClient();