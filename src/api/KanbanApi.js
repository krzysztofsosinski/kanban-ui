import fetchClient from './fetchClient';

let KanbanAPI = {
    async fetchCards() {
        return fetchClient.get('cards');
    },
    async addCard(card) {
        return fetchClient.post("cards", {...card, id: null});
    },
    async updateCard(card, draftCard) {
        return fetchClient.put(`cards/${card.id}`, draftCard);
    },
    async persistCardDrag(cardId, status, index) {
        return fetchClient.put(`cards/${cardId}`, {...status, row_order_position: index});
    },
    async addTask(cardId, task) {
        return fetchClient.post(`cards/${cardId}/tasks`, task);
    },
    async deleteTask(cardId, task) {
        return fetchClient.delete(`cards/${cardId}/tasks/${task.id}`)
    },
    async toggleTask(cardId, task) {
        return fetchClient.put(`cards/${cardId}/tasks/${task.id}`, {done: !task.done});
    },
    async singIn(email, password) {
        return fetchClient.post('auth/singin', {username: email, password});
    },
    async singUp({firstName, lastName, email, password, passwordConfirmation}) {
        return fetchClient.post('auth/signup', {firstName, lastName, email, password, passwordConfirmation});
    },
    async refresh(refreshToken) {
        return fetchClient.post('auth/refresh', {refreshToken});
    }
};

export default KanbanAPI;