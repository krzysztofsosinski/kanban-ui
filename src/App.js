import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import KanbanBoard from './components/KanbanBoard';
import SingIn from './components/auth/SingIn';
import SingOut from './components/auth/SingOut';
import SingUp from './components/auth/SingUp';
import Welcome from './components/Welcome';
import kanbanStore from './store/kanbanStore';
import {AUTH_USER} from "./actions/AuthActionCreators";
import {PrivateRoute} from "./components/auth/RequireAuth";
import CssBaseline from '@material-ui/core/CssBaseline';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';

const token = localStorage.getItem('accessToken');

if (token) {
    kanbanStore.dispatch({type: AUTH_USER});
}

const theme = createMuiTheme();

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <CssBaseline/>
                <Provider store={kanbanStore}>
                    <BrowserRouter>
                        <MuiThemeProvider theme={theme}>
                            <div>
                                <Route path="/" exact={true} component={Welcome}/>
                                <Route path="/signin" component={SingIn}/>
                                <Route path="/signout" component={SingOut}/>
                                <Route path="/signup" component={SingUp}/>
                                <PrivateRoute path="/board" component={KanbanBoard}/>
                            </div>
                        </MuiThemeProvider>
                    </BrowserRouter>
                </Provider>
            </React.Fragment>
        );
    }
}

export default App;
