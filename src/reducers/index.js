import {combineReducers} from 'redux';
import cards, * as fromCards from './cards';
import {reducer as form} from 'redux-form'
import cardDraft from './cardDraft';
import auth from './auth';

const rootReducer = combineReducers({
    cards,
    cardDraft,
    auth,
    form
});

export default rootReducer;

export const getCard = (state, id) =>
    fromCards.getCard(state.cards, id);
export const getCardIndex = (state, id) =>
    fromCards.getCardIndex(state.cards, id);